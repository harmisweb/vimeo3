Gem::Specification.new do |s|
  s.name        = 'vimeo_3'
  s.version     = '0.0.1'
  s.date        = '2015-02-01'
  s.summary     = "Vimeo API v3"
  s.description = "Vimeo API v3 Integration"
  s.authors     = ["Fernando Cordeiro"]
  s.email       = 'fernando@fernandocordeiro.com'
  s.files       = ["lib/vimeo_3.rb", "lib/vimeo_3/connect.rb"]
  s.homepage    = 'http://harmisweb.com.br'
  s.license     = 'MIT'
  end
